/* todo_common.c */

/*************************************************************************/

// Includes:

#include <linux/todo_common.h>

/*************************************************************************/

// Enums:

/*************************************************************************/

// Static Function Declarations:

static BOOL isCurrentChildByPid(pid_t pid);
static struct task_struct *pid_task(pid_t pid);
/*************************************************************************/

// Functions:

// Function Name: TODO_COMMON_isLegalTask
// Input: Current task struct pointer, PID of the task to check
// Output: Returns TRUE if the PID of the current task or its descendants matches the input PID
//         Also *pp_target_task_struct now points to the task struct of the task which pid is the input pid
int TODO_COMMON_isLegalTask(task_t *p_current, pid_t pid, task_t **pp_target_task_struct)
{
    if (pid == p_current->pid)
    {
        *pp_target_task_struct = p_current;
        return SUCCESS;
    }
    
    // first, see that this pid exists
    *pp_target_task_struct = pid_task(pid);
    if (*pp_target_task_struct == NULL)
    {
        return -ESRCH;
    }
    // then, assure it is a child process of the current process (who invoked the syscall)
    if (isCurrentChildByPid(pid) == FALSE)
    {
        return -ESRCH;
    }
    return SUCCESS;
}


static struct task_struct *pid_task(pid_t pid)
{
    task_t *p_iterator = current;
    for_each_task(p_iterator)
    {
        if (pid==p_iterator->pid)
        {
            return p_iterator;
        }
	}
    return NULL;
}

static BOOL isCurrentChildByPid(pid_t pid)
{
    task_t* p_tmp;
    if(current->pid == 0) return TRUE; // init task is ansestor for all tasks.
    task_t* p_target = pid_task(pid);
    
    for(p_tmp = p_target; p_tmp->pid; p_tmp = p_tmp->p_pptr)
        if(p_tmp == current)
            return TRUE; // Found path to current
    
    // Reach init task without touching current.
    return FALSE;
}




